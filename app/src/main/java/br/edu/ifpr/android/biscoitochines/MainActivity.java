package br.edu.ifpr.android.biscoitochines;

import android.app.ProgressDialog;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.github.kevinsawicki.http.HttpRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        new CookieTask().execute();
    }

    public void minhaSorte(View view){
        new CookieTask().execute();
    }



    private class CookieTask extends AsyncTask<Void, Void, String>{
        ProgressDialog dialog;

        @Override
        protected void onPreExecute(){
            dialog = new ProgressDialog(MainActivity.this);
            dialog.setMessage("Carregando");
            dialog.show();
        }

        @Override
        protected String doInBackground(Void... params){
            String url = Uri.parse("http://fortunecookieapi.herokuapp.com/v1/cookie").toString();
            String conteudo = HttpRequest.get(url).body();
            JSONArray jsonArray = null;
            try {
                jsonArray = new JSONArray(conteudo);
            } catch (JSONException e) {
                Log.e(getPackageName(), e.getMessage(), e);
                return null;
            }

            Log.d(getPackageName(), jsonArray.toString());
            try {
                Log.d(getPackageName(), jsonArray.getJSONObject(0).getJSONObject("fortune").getString("message").toString());
                return jsonArray.getJSONObject(0).getJSONObject("fortune").getString("message").toString();
            } catch (JSONException e) {
               Log.d(getPackageName(), "ERRO", e);
               return "ERRO";
            }
        }

        @Override
        protected void onPostExecute(String sorte){
            dialog.dismiss();
            TextView cookie = (TextView) findViewById(R.id.cookie);
            cookie.setText(sorte);
        }
    }
}
